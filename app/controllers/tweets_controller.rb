class TweetsController < ApplicationController
  def index
    @tweets = Tweet.all
  end
  def new
    @tweet = Tweet.new
  end
  def create
    message = params[:tweet][:message]
    tdate = params[:tweet][:tdate]
    @tweet = Tweet.new(message: message, tdate: tdate)
    if @tweet.save
      redirect_to '/'
    else
      render 'new'
    end
  end
  def destroy
    tweet = Tweet.find(params[:id])
    tweet.destroy
    redirect_to '/'
  end
  def edit
    @tweet = Tweet.find(params[:id])
  end
  def show
    @tweet = Tweet.find(params[:id])
  end
  def update
    tweet = Tweet.find(params[:id])
    message = params[:tweet][:message]
    tdate = params[:tweet][:tdate]
    tweet.update(message: message, tdate: tdate)
    redirect_to '/'
  end
end
